This repository contains the following educational material: 
- Operating Systems fundamentals and motivation for multithreading and concurrency.

- The basics of multithreading - how to create threads in Java as well as communicate between threads in Java.

- Performance considerations and design patterns of multithreaded  and parallel applications. Optimizing for latency or throughput.

- Data sharing between threads in Java. All the pitfalls and challenges as well as the solutions and best practices.

- Advanced lock-free algorithms and data structures for increased responsiveness and performance.

All of this material is followed by practical examples from relevant fields such as:
-  User Interface applications 
- Image Processing
- Web Applications
- Computational programs
- And others..
