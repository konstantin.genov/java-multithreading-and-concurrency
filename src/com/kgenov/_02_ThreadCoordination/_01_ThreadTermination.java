package com.kgenov._02_ThreadCoordination;
/* konstantin created on 11/16/2020 inside the package - com.kgenov._02_ThreadCoordination */

import java.math.BigInteger;

/*
Here we'll start the discussion on thread coordination with the simple task of stopping one thread from another thread.
We're gonna talk about a few ways to do it - the Thread.interrupt() method and using daemon threads
Why would we want to terminate a thread and when?
To begin with, we know that threads consume resources. Even when the thread is not doing anything it is still consuming memory and kernel resources.
Obviously, when it's running it's also consuming CPU cycles and cache memory. So, if we created a thread that already finished it's work, but the application
is still running, we would like to clean up those resources consumed by that unused thread. Another reason we'd want to stop a thread is if it's misbehaving.
It's possible that it's sending requests to a server that's not responding or simply runs a very long calculation much longer than we'd like to allow it.
The last reason we'd want to stop a thread is if we want to stop/close the entire application. By default, as long as we have one thread running, the
application will not end - even if the main thread stopped running. We want the ability to stop all threads gracefully before closing the application.
 */
public class _01_ThreadTermination {

    public static void main(String[] args) {
        // 1st example
        Thread thread = new Thread(new BlockingTask());
        thread.start(); // blocking task is running
        thread.interrupt(); // task interrupted

        // 2nd example of a long computational task
        // pass in very large numbers to create a long task
        Thread thread2 = new Thread(new LongComputationalTask(new BigInteger("2000"), new BigInteger("1000000000")));
        thread2.start();

        // the interrupt is sent, but we do not have any method or logic to handle it, so the method will not work, and thread will continue running
        // we need to find a hotspot in our code which is taking a long time and introduce logic to interrupt the thread
        thread2.interrupt(); // 1st run the code without the if statement in the for loop, then uncomment it to see the difference
    }

    private static class BlockingTask implements Runnable {

        @Override
        public void run() {
            // simulate blocking behaviour
            try {
                Thread.sleep(500000000);
            } catch (InterruptedException e) {
                System.out.println("Exiting blocking thread.");
            }
        }
    }

    private static class LongComputationalTask implements Runnable {
        private BigInteger power;
        private BigInteger base;

        public LongComputationalTask(BigInteger base, BigInteger power) {
            this.power = power;
            this.base = base;
        }

        @Override
        public void run() {
            System.out.println(base + "^" + power + " = " + pow(base, power));
        }

        private BigInteger pow(BigInteger base, BigInteger power) {
            BigInteger result = BigInteger.ONE;

            for (BigInteger i = BigInteger.ZERO; i.compareTo(power) != 0; i = i.add(BigInteger.ONE)) {
                // if statement was added as an example after unsuccessfully calling thread2.interrupt()
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("Prematurely interupted the computation!");
                    return BigInteger.ZERO;
                }
                result = result.multiply(base);
            }
            return result;
        }
    }
}
