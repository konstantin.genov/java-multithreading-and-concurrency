package com.kgenov._02_ThreadCoordination;
/* konstantin created on 11/22/2020 inside the package - com.kgenov._02_ThreadCoordination */

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class _02_ThreadJoining {
    public static void main(String[] args) throws InterruptedException {
        // list of numbers of which we want to calculate their factorial
        List<Long> inputNumbers = Arrays.asList(0L, 3435L, 35435L, 2324L, 4656L, 26L, 2435L, 5566L, 1000000000000000L);

        // factorial calculation is a cpu intensive task, which involves a lot of calculations. We'd like to use multithreading to delegate
        // the calculation of each number's factorial to a different thread. This way we can calculate all numbers in parallel

        List<FactorialThread> calculationThreads = new ArrayList<>();

        // create thread for each number
        for (long inputNumber : inputNumbers) {
            calculationThreads.add(new FactorialThread(inputNumber));
        }

        for (Thread calculationThread : calculationThreads) {
            calculationThread.setDaemon(true);
            calculationThread.start();
        }

        // resolves the race condition, instructing the main thread to wait for the threads to finish
        for (Thread calculationThread : calculationThreads) {
            calculationThread.join(2000); // how long we are willing to wait for each of the worker threads, the huge number in the inputNums list will hang the result as the number is too big
            // if a thread hasn't terminated in 2 seconds, the join method will return -> 1000000000000000L is still in progress

        }

        // this loop will cause a race conditions if we haven't joined the threads
        for (int i = 0; i < inputNumbers.size(); i++) {
            FactorialThread factorialThread = calculationThreads.get(i);
            if (factorialThread.isFinished) {
                System.out.println("Factorial of " + inputNumbers.get(i) + " is " + factorialThread.getResult());
            } else {
                System.out.println("The calculation for " + inputNumbers.get(i) + " is still in progress!");
                factorialThread.interrupt();
            }
        }
    }

    public static class FactorialThread extends Thread {
        private long inputNumber;
        private BigInteger result = BigInteger.ZERO;
        private boolean isFinished = false;

        public FactorialThread(long inputNumber) {
            this.inputNumber = inputNumber;
        }

        @Override
        public void run() {
            try {
                this.result = factorial(inputNumber);
                this.isFinished = true;
            } catch (Exception e) {
                System.out.println("Thread was interrupted.. exiting");
            }
        }

        public BigInteger factorial(long n) {
            BigInteger tempResult = BigInteger.ONE;

            for (long i = n; i > 0; i--) {
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("Thread terminated");
                    return BigInteger.ZERO;
                }
                tempResult = tempResult.multiply(new BigInteger((Long.toString(i))));
            }
            return tempResult;

        }


        public BigInteger getResult() {
            return result;
        }

        public boolean isFinished() {
            return isFinished;
        }
    }


}



