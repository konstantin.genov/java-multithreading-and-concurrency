package com.kgenov._01_ThreadCreation;
/* konstantin created on 11/11/2020 inside the package - com.kgenov._01_ThreadCreation */

public class _01_ThreadCapabilities {
    public static void main(String[] args) throws InterruptedException {
        // need to pass object which implements Runnable
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("We are now in thread: " + Thread.currentThread().getName());
                System.out.println("Current thread priority is " + Thread.currentThread().getPriority());
            }
        });

        // by default the thread will have an unhelpful name like Thread-0, which will not be very useful to us if we start creating apps with lots of thread - it will be very hard to debug
        thread.setName("New Worker Thread");

        // we can set priority to threads, as some threads need it more than others - for e.g. UI threads
        thread.setPriority(Thread.MAX_PRIORITY); // priority = 10

        // launch thread
        System.out.println("We are in thread: " + Thread.currentThread().getName() + " BEFORE starting a new thread.");
        thread.start();
        System.out.println("We are in thread: " + Thread.currentThread().getName() + " AFTER starting a new thread.");

        // puts thread to sleep - this doesn't not spin in a loop, instead the method instructs the OS to not schedule the current thread until that time passes, during that time the thread doesn't not consume CPU
        Thread.sleep(1000);



    }
}
