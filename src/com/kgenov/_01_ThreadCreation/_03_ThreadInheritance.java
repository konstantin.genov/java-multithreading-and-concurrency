package com.kgenov._01_ThreadCreation;
/* konstantin created on 11/12/2020 inside the package - com.kgenov._01_ThreadCreation */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class _03_ThreadInheritance {
    public static final int MAX_PASSWORD = 9999;

    public static void main(String[] args) {
        Random random = new Random();

        Vault vault = new Vault(random.nextInt(MAX_PASSWORD)); //randomise password on instantiation

        List<Thread> threads = new ArrayList<>();

        // create threads
        threads.add(new AscendingHackerThread(vault));
        threads.add(new DescendingHackerThread(vault));
        threads.add(new PoliceThread());

        // start the threads
        threads.forEach(Thread::start);

    }

    static class Vault {
        private int password;

        public Vault(int password) {
            this.password = password;
        }

        public boolean isCorrectPassword(int guess) throws InterruptedException {
            Thread.sleep(5);
            return this.password == guess;
        }
    }

    // by extending the thread class we can create a deeper OOP inheritance hierarchy, where we can encapsulate some common functionality for a group of threads
    private static abstract class HackerThread extends Thread {
        protected Vault vault;

        public HackerThread(Vault vault) {
            this.vault = vault;
            this.setName(this.getClass().getSimpleName()); // set thread name as class name
            this.setPriority(MAX_PRIORITY);
        }

        @Override
        public void start() {
            System.out.println("Starting thread: " + this.getName());
            super.start();
        }
    }

    // simulate brute-force password guessing
    private static class AscendingHackerThread extends HackerThread {

        public AscendingHackerThread(Vault vault) {
            super(vault);
        }

        @Override
        public void run() {
            for (int i = 0; i < MAX_PASSWORD; i++) {
                try {
                    if (vault.isCorrectPassword(i)) {
                        System.out.println(this.getName() + " guessed the password: " + i);
                        System.exit(0);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // simulate brute-force password guessing
    private static class DescendingHackerThread extends HackerThread {

        public DescendingHackerThread(Vault vault) {
            super(vault);
        }

        @Override
        public void run() {
            for (int i = MAX_PASSWORD; i > 0; i--) {
                try {
                    if (vault.isCorrectPassword(i)) {
                        System.out.println(this.getName() + " guessed the password: " + i);
                        System.exit(0);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static class PoliceThread extends Thread {
        //count from 10 to 0, sleeping for 1 second each subtraction of i
        @Override
        public void run() {
            for (int i = 10; i > 0; i--) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Seconds left before police arrival " + i);
            }
            System.out.println("Game over hackers, the police caught you!");
            System.exit(0);
        }
    }
}
