package com.kgenov._01_ThreadCreation;
/* konstantin created on 11/11/2020 inside the package - com.kgenov._01_ThreadCreation */

public class _02_ThreadExceptions {
    public static void main(String[] args) {
    // Normally, unchecked exceptions that happen in Java simply bring down the entire thread,unless we catch them explicitly and handle them in a particular way.
    Thread thread = new Thread(new Runnable() {
        @Override
        public void run() {
            System.out.println("Running thread " + Thread.currentThread().getName());
            throw new RuntimeException("Intentional!");
        }
    });

    thread.setName("Bad Thread");

    // set an exception handler for the entire thread
    thread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(Thread t, Throwable e) {
            System.out.println("Critical error happened in thread " + t.getName() + " the error is " + e.getMessage());
        }
    });

    thread.start();

    }
}
